# Why are small acids not there?
* Try LC-MS with only single acids to understand behavoir
* If we don't see them then direct injection of stds of the small acids
* Is it an MS issue? --> Then fix and try with LC again (other compounds also still fine?)

# Compare solvents
## Acid vs. basic
* 10 mM Ammonium formate (pH adjusted?) 
* MeOH (3 rep, 2 modes) - Repeat after ACN vs MeOH
* 1 % FA MeOH (3 rep, 2 modes) - Use same as below

## ACN vs. MeOH
* 1 % FA MeOH (3 rep, 2 modes)
* 1 % FA ACN (3 rep, 2 modes)

# Linearity and intensity
* Dilution series on selected conditions. This can maybe be compared untargeted. It could be that there is too much noise. Then on suspect list.
* Distribution of intensities on all compounds found for dilution experiment

# SPE
* Cleanup effect?
   * What is lost?
* Up-concentration
  * Everything is concentrated the same?
* Difference wine/must?
* Reproducibility vs. dilute and shoot

# MS optimization of intensity
* LC-MS with difference in MS settings
  * Which MS settings?
  * Interactions?

# Gradient optimization
* Spread
* Differentiation of known isomers?

# Try same on Synapt
* We still see all the compounds?

# Application
* Single wines that were used for the QC
* The Fabrezan samples

# Add Sara's std list to screening
